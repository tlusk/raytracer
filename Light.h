/*
 * Light.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef LIGHT_H_
#define LIGHT_H_

#include "LightSource.h"
#include <vector>

/**
 * Abstract class to define a light
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Light {

public:

    /**
     * Constructor for the light
     */
    Light();

    /**
     * Deconstructor for the light
     */
    virtual ~Light();

    /**
     * Gets the light sources for the light
     */
    virtual std::vector<LightSource> getLightSources() const = 0;

};

#endif /*LIGHT_H_*/
