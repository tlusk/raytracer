/*
 * AreaLight.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "AreaLight.h"
#include <cmath>

#define NUM_LIGHT_SAMPLES 4

// constructor
AreaLight::AreaLight(LightSource source, double radius) :
    source(source), radius(radius) {
}

// deconstructor
AreaLight::~AreaLight() {
}

// getLightSources
std::vector<LightSource> AreaLight::getLightSources() const {
    double numLights= NUM_LIGHT_SAMPLES;
    double sampleDistance = 1.0/numLights;
    std::vector<LightSource> lightSources;

    Point3d position = source.getPosition();
    for (double x = 0; x < 1.0; x+=sampleDistance) {
        for (double y = 0; y < 1.0; y+=sampleDistance) {
            for (double z = 0; z < 1.0; z+=sampleDistance) {
                double randomX = (rand()/static_cast<double>(RAND_MAX))*sampleDistance;
                double randomY = (rand()/static_cast<double>(RAND_MAX))*sampleDistance;
                double randomZ = (rand()/static_cast<double>(RAND_MAX))*sampleDistance;

                double xOffset = (-0.5+x+randomX) * radius;
                double yOffset = (-0.5+y+randomY) * radius;
                double zOffset = (-0.5+z+randomZ) * radius;

                LightSource newLight(Vector3d(xOffset+position.X(), yOffset+position.Y(), zOffset+position.Z()), source.getColor() / std::pow(numLights, 3));
                lightSources.push_back(newLight);
            }
        }
    }

    return lightSources;
}
