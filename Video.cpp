/*
 * Video.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "Video.h"

// constructor
Video::Video(int h_rez, int v_rez) {
    surf = init_sdl(h_rez, v_rez);
    init_opengl(h_rez, v_rez);
}

// deconstructor
Video::~Video() {
    SDL_FreeSurface(surf);
    SDL_Quit();
}

// init_opengl
void Video::init_opengl(long width, long height) {
    // Set projection type and size
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    gluPerspective(45, (GLfloat)width/height, 1, 1000);

    /*
     * Disable stuff that's likely to slow down glDrawPixels.
     */
    glDisable(GL_ALPHA_TEST);
    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DITHER);
    glDisable(GL_FOG);
    glDisable(GL_LIGHTING);
    glDisable(GL_LOGIC_OP);
    glDisable(GL_STENCIL_TEST);
    glDisable(GL_TEXTURE_1D);
    glDisable(GL_TEXTURE_2D);
    glPixelTransferi(GL_MAP_COLOR, GL_FALSE);
    glPixelTransferi(GL_RED_SCALE, 1);
    glPixelTransferi(GL_RED_BIAS, 0);
    glPixelTransferi(GL_GREEN_SCALE, 1);
    glPixelTransferi(GL_GREEN_BIAS, 0);
    glPixelTransferi(GL_BLUE_SCALE, 1);
    glPixelTransferi(GL_BLUE_BIAS, 0);
    glPixelTransferi(GL_ALPHA_SCALE, 1);
    glPixelTransferi(GL_ALPHA_BIAS, 0);

    /*
     * Disable extensions that could slow down glDrawPixels.
     */
    const GLubyte* extString = glGetString(GL_EXTENSIONS);

    if (extString != NULL) {
        if (strstr((char*) extString, "GL_EXT_convolution") != NULL) {
            glDisable(GL_CONVOLUTION_1D_EXT);
            glDisable(GL_CONVOLUTION_2D_EXT);
            glDisable(GL_SEPARABLE_2D_EXT);
        }
        if (strstr((char*) extString, "GL_EXT_histogram") != NULL) {
            glDisable(GL_HISTOGRAM_EXT);
            glDisable(GL_MINMAX_EXT);
        }
        if (strstr((char*) extString, "GL_EXT_texture3D") != NULL) {
            glDisable(GL_TEXTURE_3D_EXT);
        }
    }

    glClearColor(0.0, 0.0, 0.0, 1.0);
}

// init_sdl
SDL_Surface *Video::init_sdl(long width, long height) {
    SDL_Init(SDL_INIT_VIDEO);
    SDL_WM_SetCaption("CG2 Raytracer", 0);

    const SDL_VideoInfo* info = SDL_GetVideoInfo();
    int flags= SDL_OPENGL | SDL_GL_DOUBLEBUFFER | SDL_HWSURFACE;
    return SDL_SetVideoMode(width, height, info->vfmt->BitsPerPixel, flags);
}
