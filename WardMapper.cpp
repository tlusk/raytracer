/*
 * WardMapper.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "WardMapper.h"
#include <cmath>

// constructor
WardMapper::WardMapper(double lMax, double ldMax) :
    ToneMapper(lMax, ldMax) {
}

// deconstructor
WardMapper::~WardMapper() {
}

// compressColorLevels
Color WardMapper::compressColorLevels(Color color, double lAvg) const {
    double sf = std::pow((1.219 + std::pow(ldMax / 2.0, 0.4)) / (1.219 + std::pow(lAvg, 0.4)), 2.5);

    return color * sf;
}
