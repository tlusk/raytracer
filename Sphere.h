/*
 * Sphere.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef SPHERE_H_
#define SPHERE_H_

#include "Object.h"

/**
 * Sphere class, used to draw spheres of any size
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Sphere : public Object {

public:

    /**
     * Constructor for the sphere class
     * 
     * @param x the x coordinate of the sphere
     * @param y the y coordinate of the sphere
     * @param z the z coordinate of the sphere
     * @param r the radius of the sphere
     * @param material The material the object is created from, contains texture and illumination
     * @param kReflection reflection constant for the object
     * @param kTransmission transmission constant for the object
     * @param kRefraction refraction constant for the object
     */
    Sphere(double x, double y, double z, double r, IlluminationModel* material, double kReflection, double kTransmission, double kRefraction);

    /**
     * Deconstructor for the sphere class
     */
    virtual ~Sphere();

    /**
     * Get the intersection point for the sphere
     * 
     * @param ray the to calculate an intersection with
     * @return intersection data about the collision
     */
    virtual Intersection getIntersection(Ray3d ray) const;

private:

    /**
     * The center point for the sphere
     */
    Point3d center;

    /**
     * The radius of the sphere
     */
    double radius;

};

#endif /*SPHERE_H_*/
