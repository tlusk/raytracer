/*
 * PhongModel.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef PHONGMODEL_H_
#define PHONGMODEL_H_

#include "IlluminationModel.h"

/**
 * Class used to define the phone illumination model
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class PhongModel : public IlluminationModel {

public:

    /**
     * Construct a phone illumination model from a texture, will also be used as the diffuse texture, specular will be set to solid white
     * 
     * @param objectTexture the texture to use for this illumination
     * @param ambientCoefficient how bright the ambient color is
     * @param diffuseCoefficient how bright the diffuse color is
     * @param specularCoefficient how bright the specular color is
     * @param shine the intensity of the specular shine
     */
    PhongModel(Texture* objectColor, double ambientCoefficient, double diffuseCoefficient, double specularCoefficient, double shine);

    /**
     * Construct a phone illumination model from an ambient, diffuse, and specular texture
     * 
     * @param ambientTexture ambientTexture to use for this illumination
     * @param diffuseTexture diffuseTexture to use for this illumination
     * @param specularTexture specularTexture to use for this illumination
     * @param ambientCoefficient how bright the ambient color is
     * @param diffuseCoefficient how bright the diffuse color is
     * @param specularCoefficient how bright the specular color is
     * @param shine the intensity of the specular shine
     */
    PhongModel(Texture* ambientColor, Texture* diffuseColor, Texture* specularColor, double ambientCoefficient, double diffuseCoefficient, double specularCoefficient, double shine);

    /**
     * Deconstructor for the phong model
     */
    virtual ~PhongModel();

    /**
     * Returns the illuminated color at a specified intersection with passed light sources
     * 
     * @param intersectionData the intersection point
     * @param lights the lights acting on the intersection point
     * @return the color of the illuminated point
     */
    virtual Color illuminate(Intersection intersectionData, std::vector<LightSource> lights) const;

private:

    /**
     * Holds how bright the ambient color is
     */
    double ambientCoefficient;

    /**
     * Holds how bright the diffuse color is
     */
    double diffuseCoefficient;

    /**
     * Holds how bright the specular color is
     */
    double specularCoefficient;

    /**
     * Holds how intense the specular shine is
     */
    double shine;

};

#endif /*PHONGMODEL_H_*/
