/*
 * Raytracer.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "Raytracer.h"
#include "Sphere.h"
#include "Polygon.h"
#include "PhongModel.h"
#include "CheckerboardTexture.h"
#include "SolidTexture.h"
#include "ImageTexture.h"
#include "PointLight.h"
#include "AreaLight.h"

// constructor
Raytracer::Raytracer(int xResolution, int yResolution, int numSuperSamples) :
    video(new Video(xResolution, yResolution)), world(new World()), renderer(new Renderer(*world, xResolution, yResolution, numSuperSamples)) {

    // Initalize Random Number Generator
    srand(time(NULL) );

    init();
}

// deconstructor
Raytracer::~Raytracer() {
    delete renderer;
    delete world;
    delete video;
}

// init
void Raytracer::init() {
    double Ka = 0.3;
    double Kd = 0.7;
    double Ks = 0.8;
    double shine = 100.0;

    Object* sphere1 = new Sphere(-1.5, 0.5, 4.0, 0.4, new PhongModel(new SolidTexture(Color(0.7,0.7,0.7)), 0.15, 0.25, 1.0, 20.0), 0.75, 0.0, 1.0);
    Object* sphere2 = new Sphere(-2.1, 1.0, 5.0, 0.4, new PhongModel(new SolidTexture(Color(1.0,1.0,1.0)), 0.075, 0.075, 0.2, 20.0), 0.01, 0.85, 0.95);
    //Object* floor = new Polygon(Point3d(-4.0, 0.0, -10.0), Point3d(-4.0, 0.0, 10.0), Point3d(4.0, 0.0, 10.0), Point3d(4.0, 0.0, -10.0), new PhongModel(new ImageTexture("grass.bmp"), Ka, Kd, Ks, shine));
    Object* floor = new Polygon(Point3d(-4.0, 0.0, -10.0), Point3d(-4.0, 0.0, 10.0), Point3d(4.0, 0.0, 10.0), Point3d(4.0, 0.0, -10.0), new PhongModel(new CheckerboardTexture(Color(1,0,0),Color(1,1,0)), Ka, Kd, Ks, shine), 0.0, 0.0, 1.0);
    Light* light = new PointLight(LightSource(Point3d(-3.0, 2.0, 9.0), Color(1.0,1.0,1.0)));
    //Light* light = new AreaLight(LightSource(Point3d(-3.0, 2.0, 9.0), Color(1.0,1.0,1.0)),0.7);
    //Light* light2 = new PointLight(LightSource(Point3d(-1.0, 2.0, 9.0), Color(1.0,1.0,1.0)));

    world->add(sphere1);
    world->add(sphere2);
    world->add(floor);
    world->add(light);
    //world.add(light2);
}

// run
void Raytracer::run() {
    // Pre-render the scene
    renderer->trace();

    // main loop variable
    int done= false;

    // used to collect sdl events
    SDL_Event event;

    // wait for events
    while (!done) {

        // handle the events in the queue
        while (SDL_PollEvent(&event)) {

            switch (event.type) {
            case SDL_QUIT:
                // Quit
                done = true;
                break;
            default:
                break;
            }
        }

        // Render the scene
        renderer->calculateFps();
        renderer->render();

        // Delay so the CPU isn't 100% (FPS doesn't really matter)
        SDL_Delay(100);
    }
}
