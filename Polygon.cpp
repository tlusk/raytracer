/*
 * Polygon.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "Polygon.h"
#include <cmath>

#define EPSILON  0.0000001

// constructor
Polygon::Polygon(Point3d v0, Point3d v1, Point3d v2, Point3d v3, IlluminationModel* material, double kReflection, double kTransmission, double kRefraction) :
    Object(material, kReflection, kTransmission, kRefraction), plane(v0, v1, v2) {
    vertices.push_back(v0);
    vertices.push_back(v1);
    vertices.push_back(v2);
    vertices.push_back(v3);
}

// deconstructor
Polygon::~Polygon() {
}

// getIntersection
Intersection Polygon::getIntersection(Ray3d ray) const {
    Point3d rayOrigin = ray.getOrigin();
    Vector3d rayDirection = ray.getDirection();
    Vector3d planeNormal = plane.getNormal();

    double intersectionIndicator = rayDirection * planeNormal;

    if (intersectionIndicator != 0) {
        double omegaTemp = (planeNormal * rayOrigin) + plane.signedDistanceTo(Vector3d(0, 0, 0));
        double omega = (-omegaTemp) / intersectionIndicator;
        if (omega > 0) {
            Point3d intersectionPoint = rayOrigin + ( rayDirection * omega );
            double angleSum = CalcAngleSum(intersectionPoint, vertices);
            if (fabs(angleSum - (M_PI * 2)) < EPSILON) {
                return Intersection(omega, rayOrigin, intersectionPoint, planeNormal);
            }
        }
    }
    return Intersection(-1, Point3d(0,0,0), Point3d(-1,-1,-1), Vector3d(-1, -1, -1));
}

// CalcAngleSum
double Polygon::CalcAngleSum(Point3d q, std::vector<Point3d> points) const {
    double m1, m2;
    double anglesum=0, costheta;
    Point3d p1, p2;

    for (size_t point = 0; point < points.size(); point++) {
        p1 = points.at(point) - q;
        p2 = points.at((point+1)%points.size()) - q;

        m1 = p1.module();
        m2 = p2.module();
        if (m1*m2 <= EPSILON)
            return (M_PI * 2); // We are on a node, consider this inside
        else
            costheta = (p1*p2)/(m1*m2);

        anglesum += acos(costheta);
    }
    return (anglesum);
}
