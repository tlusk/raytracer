/*
 * Intersection.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "Intersection.h"

// constructor
Intersection::Intersection(double intersectionDistance, Point3d cameraPosition, Point3d intersectionPoint, Vector3d normal) :
    intersectionDistance(intersectionDistance), intersectionPoint(intersectionPoint), viewingDirection(cameraPosition-intersectionPoint), normal(normal) {
    viewingDirection.normalize();
    this->normal.normalize();
}

// deconstructor
Intersection::~Intersection() {
}

// getIntersectionPoint
Point3d Intersection::getIntersectionPoint() const {
    return intersectionPoint;
}

// getNormal
Vector3d Intersection::getNormal() const {
    return normal;
}

// getViewingDirection
Vector3d Intersection::getViewingDirection() const {
    return viewingDirection;
}

// getIntersectionDistance
double Intersection::getIntersectionDistance() const {
    return intersectionDistance;
}
