/*
 * Color.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef COLOR_H_
#define COLOR_H_

/**
 * BMP Texture class, loads a texture from a bitmap file, colors can then be extracted
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Color {

public:

    /**
     * Default Constructor, content is unknown and set to black
     */
    Color();

    /**
     * Constructs a color object from red green and blue values
     * 
     * @param r the amount of red in this color
     * @param g the amount of green in this color
     * @param b the amount of blue in this color
     */
    Color(double r, double g, double b);

    /**
     * Deconstructor for the color object
     */
    virtual ~Color();

    /**
     * Get the amount of red in the color
     * 
     * @return the amount of red in the color
     */
    double R() const;

    /**
     * Get the amount of green in the color
     * 
     * @return the amount of green in the color
     */
    double G() const;

    /**
     * Get the amount of blue in the color
     * 
     * @return the amount of blue in the color
     */
    double B() const;

    /**
     * Clamp the color values so that no color can ever be negative
     * 
     * @param colorValue the original color value
     * @return the clamped color value
     */
    double clamp(double colorValue) const;

    // Helper operations
    Color operator+=(const Color &c);
    Color operator*=(const Color &c);
    Color operator*=(double n);

    friend Color operator+(const Color &a, const Color &b);
    friend Color operator*(const Color &a, const Color &b);
    friend Color operator/(const Color &a, const Color &b);

    friend Color operator+(const Color &a, double n);
    friend Color operator*(const Color &a, double n);
    friend Color operator/(const Color &a, double n);
    friend Color operator+(double n, const Color &a);
    friend Color operator*(double n, const Color &a);
    friend Color operator/(double n, const Color &a);

private:

    /**
     * Amount of red in this color
     */
    double r;

    /**
     * Amount of green in this color
     */
    double g;

    /**
     * Amount of green in this color
     */
    double b;

};

#endif /*COLOR_H_*/
