#ifndef VECTOR4_H_
#define VECTOR4_H_

#include <string>
#include <cmath>

/**
 Do not subclass-- this implementation makes assumptions about the
 memory layout.
 */
class Vector4 {
private:
    // Hidden operators
    bool operator<(const Vector4&) const;
    bool operator>(const Vector4&) const;
    bool operator<=(const Vector4&) const;
    bool operator>=(const Vector4&) const;

public:
    // construction
    Vector4();
    Vector4(float fX, float fY, float fZ, float fW);
    Vector4(float afCoordinate[4]);
    Vector4(const Vector4& rkVector);

    // coordinates
    float x, y, z, w;

    // access vector V as V[0] = V.x, V[1] = V.y, V[2] = V.z, etc.
    //
    // WARNING.  These member functions rely on
    // (1) Vector4 not having virtual functions
    // (2) the data packed in a 4*sizeof(float) memory block
    float& operator[] (int i);
    const float& operator[] (int i) const;
    operator float* ();
    operator const float* () const;

    // assignment and comparison
    Vector4& operator= (const Vector4& rkVector);
    bool operator== (const Vector4& rkVector) const;
    bool operator!= (const Vector4& rkVector) const;

    inline void set(float _x, float _y, float _z, float _w) {
        x = _x;
        y = _y;
        z = _z;
        w = _w;
    }

    size_t hashCode() const;
    bool fuzzyEq(const Vector4& other) const;
    bool fuzzyNe(const Vector4& other) const;

    /** sqrt(this->dot(*this)) */
    float length() const;
    float squaredLength() const;

    inline float sum() const {
        return x + y + z + w;
    }

    /** Returns true if this vector has finite length */
    bool isFinite() const;

    /** Returns true if this vector has length == 0 */
    bool isZero() const;

    /** Returns true if this vector has length == 1 */
    bool isUnit() const;

    // arithmetic operations
    Vector4 operator+ (const Vector4& rkVector) const;
    Vector4 operator- (const Vector4& rkVector) const;
    
    inline Vector4 operator*(const Vector4& rkVector) const {
        return Vector4(x * rkVector.x, y * rkVector.y, z * rkVector.z, w * rkVector.w);
    }

    inline Vector4 operator/(const Vector4& rkVector) const {
        return Vector4(x / rkVector.x, y / rkVector.y, z / rkVector.z, w / rkVector.w);
    }

    Vector4 operator*(const class Matrix4& M) const;

    Vector4 operator* (float fScalar) const;
    Vector4 operator/ (float fScalar) const;
    Vector4 operator- () const;
    friend Vector4 operator* (float, const Vector4& rkVector);

    // arithmetic updates
    Vector4& operator+= (const Vector4& rkVector);
    Vector4& operator-= (const Vector4& rkVector);
    Vector4& operator*= (float fScalar);
    Vector4& operator/= (float fScalar);

    float dot (const Vector4& rkVector) const;

    Vector4 min(const Vector4& v) const;
    Vector4 max(const Vector4& v) const;

    std::string toString() const;

    /**
     Linear interpolation
     */
    Vector4 lerp(const Vector4& v, float alpha) const;

};


//----------------------------------------------------------------------------
inline Vector4::Vector4() {
    x = y = z = w = 0;
}

//----------------------------------------------------------------------------

inline Vector4::Vector4 (float fX, float fY, float fZ, float fW) {
    x = fX;
    y = fY;
    z = fZ;
    w = fW;
}

//----------------------------------------------------------------------------
inline Vector4::Vector4 (float afCoordinate[4]) {
    x = afCoordinate[0];
    y = afCoordinate[1];
    z = afCoordinate[2];
    w = afCoordinate[3];
}

//----------------------------------------------------------------------------
inline Vector4::Vector4(const Vector4& rkVector) {
    x = rkVector.x;
    y = rkVector.y;
    z = rkVector.z;
    w = rkVector.w;
}

//----------------------------------------------------------------------------
inline float& Vector4::operator[] (int i) {
    return ((float*)this)[i];
}

//----------------------------------------------------------------------------
inline const float& Vector4::operator[] (int i) const {
    return ((float*)this)[i];
}

//----------------------------------------------------------------------------
inline Vector4::operator float* () {
    return (float*)this;
}

inline Vector4::operator const float* () const {
    return (float*)this;
}

//----------------------------------------------------------------------------
inline Vector4& Vector4::operator= (const Vector4& rkVector) {
    x = rkVector.x;
    y = rkVector.y;
    z = rkVector.z;
    w = rkVector.w;
    return *this;
}

//----------------------------------------------------------------------------
inline bool Vector4::operator== (const Vector4& rkVector) const {
    return ( (x == rkVector.x) && (y == rkVector.y) && (z == rkVector.z) && (w == rkVector.w));
}

//----------------------------------------------------------------------------
inline bool Vector4::operator!= (const Vector4& rkVector) const {
    return ( x != rkVector.x || y != rkVector.y || z != rkVector.z || w != rkVector.w);
}

//----------------------------------------------------------------------------
inline Vector4 Vector4::operator+ (const Vector4& rkVector) const {
    return Vector4(x + rkVector.x, y + rkVector.y, z + rkVector.z, w + rkVector.w);
}

//----------------------------------------------------------------------------
inline Vector4 Vector4::operator- (const Vector4& rkVector) const {
    return Vector4(x - rkVector.x, y - rkVector.y, z - rkVector.z, w - rkVector.w);
}

//----------------------------------------------------------------------------
inline Vector4 Vector4::operator* (float fScalar) const {
    return Vector4(fScalar*x, fScalar*y, fScalar*z, fScalar*w);
}

//----------------------------------------------------------------------------
inline Vector4 Vector4::operator- () const {
    return Vector4( -x, -y, -z, -w);
}

//----------------------------------------------------------------------------
inline Vector4& Vector4::operator+= (const Vector4& rkVector) {
    x += rkVector.x;
    y += rkVector.y;
    z += rkVector.z;
    w += rkVector.w;
    return *this;
}

//----------------------------------------------------------------------------
inline Vector4& Vector4::operator-= (const Vector4& rkVector) {
    x -= rkVector.x;
    y -= rkVector.y;
    z -= rkVector.z;
    w -= rkVector.w;
    return *this;
}

//----------------------------------------------------------------------------

inline Vector4 Vector4::lerp(const Vector4& v, float alpha) const {
    return (*this) + (v - *this) * alpha; 
}


//----------------------------------------------------------------------------
inline Vector4& Vector4::operator*= (float fScalar) {
    x *= fScalar;
    y *= fScalar;
    z *= fScalar;
    w *= fScalar;
    return *this;
}


//----------------------------------------------------------------------------
inline float Vector4::dot(const Vector4& rkVector) const {
    return x*rkVector.x + y*rkVector.y + z*rkVector.z + w*rkVector.w;
}

//----------------------------------------------------------------------------
inline bool Vector4::isZero() const {
    return (x == 0.0f) && (y == 0.0f) && (z == 0.0f) && (w == 0.0f);
}

//----------------------------------------------------------------------------

inline bool Vector4::isUnit() const {
    return squaredLength() == 1.0;
}

//----------------------------------------------------------------------------

inline float Vector4::length() const {
    return sqrtf(squaredLength());
}

//----------------------------------------------------------------------------

inline float Vector4::squaredLength() const {
    return x * x + y * y + z * z + w * w;
}

#endif /*VECTOR4_H_*/
