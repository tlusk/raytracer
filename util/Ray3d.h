/*
 * Ray3d.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef RAY3D_H_
#define RAY3D_H_

#include "Vector3d.h"

/**
 * Ray3d class, holds the information used to describe a ray
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Ray3d {

public:

    /**
     * Create a 3d ray from coordinates
     * 
     * @param x x origin for the ray
     * @param y y origin for the ray
     * @param z z origin for the ray
     * @param dx x direction for the ray
     * @param dy y direction for the ray
     * @param dz z direction for the ray
     */
    Ray3d(double x, double y, double z, double dx, double dy, double dz);

    /**
     * Create a 3d ray from an origin and a direction
     * 
     * @param origin the origin of the ray
     * @param direction the direction of the ray
     */
    Ray3d(Point3d origin, Vector3d direction);

    /**
     * Deconstructor for the Ray3d class
     */
    virtual ~Ray3d();

    /**
     * Gets the origin of the ray
     * 
     * @return the origin of the ray
     */
    Point3d getOrigin() const;

    /**
     * Gets the direction of the ray
     * 
     * @return the direction of the ray
     */
    Vector3d getDirection() const;

private:

    /**
     * The origin of the ray
     */
    Point3d origin;

    /**
     * The direction of the ray
     */
    Vector3d direction;

};

#endif /*RAY3D_H_*/
