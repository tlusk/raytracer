#include "Vector4.h"

size_t Vector4::hashCode() const {
    unsigned int xhash = (*(int*)(void*)(&x));
    unsigned int yhash = (*(int*)(void*)(&y));
    unsigned int zhash = (*(int*)(void*)(&z));
    unsigned int whash = (*(int*)(void*)(&w));

    return xhash + (yhash * 37) + (zhash * 101) + (whash * 241);
}

Vector4 Vector4::operator/ (float fScalar) const {
    Vector4 kQuot;

    if ( fScalar != 0.0 ) {
		float fInvScalar = 1.0f / fScalar;
        kQuot.x = fInvScalar * x;
        kQuot.y = fInvScalar * y;
        kQuot.z = fInvScalar * z;
        kQuot.w = fInvScalar * w;
        return kQuot;
    } else {
        return kQuot;
    }
}

//----------------------------------------------------------------------------
Vector4& Vector4::operator/= (float fScalar) {
    if (fScalar != 0.0f) {
		float fInvScalar = 1.0f / fScalar;
        x *= fInvScalar;
        y *= fInvScalar;
        z *= fInvScalar;
        w *= fInvScalar;
    } else {
		//*this = Vector4::inf();
    }

    return *this;
}
