#ifndef VECTOR3D_H_
#define VECTOR3D_H_

#include <math.h>
#include <string>
#include <iostream>

#define Point3d Vector3d

/** 
 * A Vector in a 3d coordinate system, with the product and dividion
 * with doubles, product, sum, difference of vectors, assignation
 * to string, and conversion to polar systems.
 *
 * Point3d is an alias of Vector3d.
 */
class Vector3d {
private:
	/** The coordinate of the vector*/
	double x, y, z;
public:
	Vector3d(double _x, double _y, double _z);
	/** A convenience contructor, content is unknown */
	Vector3d();

	Vector3d operator=(const Vector3d &v);
	Vector3d operator-=(const Vector3d &v);
	Vector3d operator+=(const Vector3d &v);
	Vector3d operator*=(const Vector3d &v);
	Vector3d operator/=(const Vector3d &v);
	Vector3d operator*=(double);
	Vector3d operator/=(double);

	/// Rotate in any given axis
	Vector3d rotate(const Vector3d &v, double ang);
	double module(void);
	/// Distance to a given plane
	double distToPlane(const Point3d &p, const Vector3d &n);
	/// Distance to a given line
	double distToLine(const Point3d &p, const Vector3d &d);
	/// Returns the current vector, normalized
	Vector3d normalized(void);
	/// Normalize current vector
	void normalize(void);

	/// Converts the Vector3d to a string
	std::string toString(void);

	/** 
	 * @name Get components
	 * @{
	 */
	double X() const;
	double Y() const;
	double Z() const;
	/** @} */

	friend bool operator==(const Vector3d &a, const Vector3d &b);
	friend Vector3d operator^(const Vector3d &a, const Vector3d &b);
	friend double operator*(const Vector3d &a, const Vector3d &b);
	friend Vector3d operator-(const Vector3d &a, const Vector3d &b);
	friend Vector3d operator+(const Vector3d &a, const Vector3d &b);

	friend Vector3d operator/(const Vector3d &a, double n);
	friend Vector3d operator*(const Vector3d &a, double n);
	friend Vector3d operator/(double n, const Vector3d &a);
	friend Vector3d operator*(double n, const Vector3d &a);

	friend Vector3d operator-(const Vector3d &v);
};

#endif /*VECTOR3D_H_*/
