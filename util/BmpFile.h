/*
 * BmpFile.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef BMPFILE_H_
#define BMPFILE_H_

#include <iostream>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "Color.h"

/**
 * BMP Texture class, loads a texture from a bitmap file, colors can then be extracted
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class BmpFile {

public:

    /**
     * Constructor for the texture class
     * 
     * @param textureName fileName of the texture to load
     */
    BmpFile(std::string textureName);

    /**
     * Deconstructor for the texture class
     */
    virtual ~BmpFile();

    /**
     * Get the pixel height of the bitmap file ( y resolution )
     * 
     * @return the height of the bitmap file
     */
    int getHeight() const;

    /**
     * Get the pixel width of the bitmap file ( x resolution )
     * 
     * @return the width of the bitmap file
     */
    int getWidth() const;

    /**
     * Get the pixel color for a specified x and y location in the image
     * 
     * @param x the xCoordinate of the pixel
     * @param y the yCoordinate of the pixel
     * @return the color of the pixel
     */
    Color getPixel(int x, int y) const;

private:

    /**
     * SDL Surface used to store the images
     */
    SDL_Surface* image;

};

#endif /*BMPFILE_H_*/
