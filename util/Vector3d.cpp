#include "Vector3d.h"

#include <math.h>
#include <iostream>
#include <stdio.h>
#include <sstream>

Vector3d::Vector3d(double _x, double _y, double _z) {
	x=_x;
	y=_y;
	z=_z;
}

Vector3d::Vector3d() {

}

std::string Vector3d::toString(void) {

	std::stringstream ss;
	std::string str;
	ss << "("<< x << ","<< y << ","<< z << ")";
	ss >> str;

	return str;
}

Vector3d Vector3d::operator=(const Vector3d &b) {
	x=b.x; y=b.y; z=b.z;
	return *this;
}

Vector3d Vector3d::operator+=(const Vector3d &v) {
	x+=v.x; y+=v.y; z+=v.z;
	return *this;
}

Vector3d Vector3d::operator-=(const Vector3d &v) {
	x-=v.x; y-=v.y; z-=v.z;
	return *this;
}

Vector3d Vector3d::operator*=(double d) {
	x*=d; y*=d; z*=d;
	return *this;
}

Vector3d Vector3d::operator/=(double d) {
	x/=d; y/=d; z/=d;
	return *this;
}

double Vector3d::module(void) {
	return sqrt(x*x+y*y+z*z);
}

Vector3d Vector3d::normalized(void) {
	double m=module();
	return Vector3d(x/m, y/m, z/m);
}

void Vector3d::normalize(void) {
	double m=module();
	x/=m;
	y/=m;
	z/=m;
}

/**
 * Rotates current vector over a given axis, a given ammount. The
 * formula used is a solved version of quaternion rotations.
 */
Vector3d Vector3d::rotate(const Vector3d &v, double ang) {
	// First we calculate [w,x,y,z], the rotation quaternion
	double w, x, y, z;
	Vector3d V=v;
	V.normalize();
	w=cos(-ang/2); // The formula rotates counterclockwise, and I
	// prefer clockwise, so I change 'ang' sign
	double s=sin(-ang/2);
	x=V.x*s;
	y=V.y*s;
	z=V.z*s;
	// now we calculate [w^2, x^2, y^2, z^2]; we need it
	double w2=w*w;
	double x2=x*x;
	double y2=y*y;
	double z2=z*z;

	// And apply the formula
	return Vector3d((*this).x*(w2+x2-y2-z2)+ (*this).y*2*(x*y+w*z)+ (*this).z*2*(x*z-w*y), (*this).x*2*(x*y-w*z)+ (*this).y*(w2-x2+y2-z2)+ (*this).z*2*(y*z+w*x), (*this).x*2*(x*z+w*y)+ (*this).y*2*(y*z-w*x)+ (*this).z*(w2-x2-y2+z2));
}

double Vector3d::X() const {
	return x;
}

double Vector3d::Y() const {
	return y;
}

double Vector3d::Z() const {
	return z;
}

double Vector3d::distToPlane(const Point3d &p, const Vector3d &n) {
	Vector3d N=n;
	N.normalize();

	return fabs(N*(*this-p));
}

double Vector3d::distToLine(const Point3d &p, const Vector3d &d) {
	Vector3d v=(*this-p);
	Vector3d D=d;
	return (D^v).module()/D.module();
}

//// These operators are just friends to facilitate the use with const&

bool operator==(const Vector3d &a, const Vector3d &b) {
	return (a.x==b.x)&&(a.y==b.y)&&(a.z==b.z);
}

double operator*(const Vector3d &a, const Vector3d &b) {
	return a.x*b.x+ a.y*b.y+ a.z*b.z;
}

Vector3d operator^(const Vector3d &a, const Vector3d &b) {
	return Vector3d(a.y*b.z-a.z*b.y, -a.x*b.z+a.z*b.x, a.x*b.y-a.y*b.x);
}

Vector3d operator+(const Vector3d &a, const Vector3d &b) {
	return Vector3d(a.x+b.x, a.y+b.y, a.z+b.z);
}

Vector3d operator-(const Vector3d &a, const Vector3d &b) {
	return Vector3d(a.x-b.x, a.y-b.y, a.z-b.z);
}

Vector3d operator-(const Vector3d &a) {
	return Vector3d(-a.x, -a.y, -a.z);
}

Vector3d operator*(const Vector3d &a, double d) {
	return Vector3d(a.x*d, a.y*d, a.z*d);
}

Vector3d operator/(const Vector3d &a, double d) {
	return Vector3d(a.x/d, a.y/d, a.z/d);
}
Vector3d operator*(double d, const Vector3d &a) {
	return Vector3d(a.x*d, a.y*d, a.z*d);
}

Vector3d operator/(double d, const Vector3d &a) {
	return Vector3d(a.x/d, a.y/d, a.z/d);
}
