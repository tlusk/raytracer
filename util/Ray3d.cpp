/*
 * Ray3d.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "Ray3d.h"

// constructor from coordinates
Ray3d::Ray3d(double x, double y, double z, double dx, double dy, double dz) :
    origin(x, y, z), direction(dx, dy, dz) {
    direction.normalize();
}

// constructor from point and vector
Ray3d::Ray3d(Point3d origin, Vector3d direction) :
    origin(origin), direction(direction) {
}

// deconstructor
Ray3d::~Ray3d() {
}

// getOrigin
Point3d Ray3d::getOrigin() const {
    return origin;
}

// getDirection
Vector3d Ray3d::getDirection() const {
    return direction;
}
