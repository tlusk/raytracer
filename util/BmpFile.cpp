/*
 * BmpFile.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "BmpFile.h"

// Define for windows because the library is missing this for some reason
#ifdef WIN32
#define GL_BGR					0x80E0
#define GL_BGRA					0x80E1
#endif

// constructor
BmpFile::BmpFile(std::string textureName) {
	image = SDL_LoadBMP(textureName.c_str());
}

// deconstructor
BmpFile::~BmpFile() {
}

// getHeight
int BmpFile::getHeight() const {
	return image->h;
}

// getWidth
int BmpFile::getWidth() const {
	return image->w;
}

// getPixel
Color BmpFile::getPixel(int x, int y) const {
	int bpp = image->format->BytesPerPixel;
	Uint8 *p = (Uint8 *)image->pixels + y * image->pitch+ x * bpp;
	Uint32 pixel;
	switch (bpp) {
	case 1:
		pixel = *p;
		break;
	case 2:
		pixel = *(Uint16 *)p;
		break;
	case 3:
		if (SDL_BYTEORDER==SDL_BIG_ENDIAN) {
			pixel = p[0] << 16| p[1] << 8| p[2];
		} else {
			pixel = p[0] | p[1] << 8| p[2] << 16;
		}
		break;
	case 4:
		pixel = *(Uint32 *)p;
		break;
	default:
		pixel = 0;
		break;
	}

	Uint8 r, g, b;
	SDL_GetRGB(pixel, image->format, &r, &g, &b);

	return Color(r/255.0, g/255.0, b/255.0);
}
