/*
 * Color.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "Color.h"
#include <iostream>

// default constructor
Color::Color() :
	r(0.0), g(0.0), b(0.0) {
}

// constructor
Color::Color(double r, double g, double b) :
	r(r), g(g), b(b) {
}

// deconstructor
Color::~Color() {
}

// R
double Color::R() const {
	return clamp(r);
}

// G
double Color::G() const {
	return clamp(g);
}

// B
double Color::B() const {
	return clamp(b);
}

// clamp
double Color::clamp(double colorValue) const {
	colorValue = std::max(0.0, colorValue);
	//colorValue = std::min(colorValue, 1.0);
	
	return colorValue;
}

Color Color::operator+=(const Color &c) {
	r+=c.r; g+=c.g; b+=c.b;
	
	return *this;
}

Color Color::operator*=(const Color &c) {
	r*=c.r; g*=c.g; b*=c.b;
	
	return *this;
}

Color Color::operator*=(double n) {
	r*=n; g*=n; b*=n;
	
	return *this;
}

Color operator+(const Color &a, const Color &b) {
	return Color(a.R() + b.R(), a.G() + b.G(), a.B() + b.B());
}

Color operator*(const Color &a, const Color &b) {
	return Color(a.R() * b.R(), a.G() * b.G(), a.B() * b.B());
}

Color operator/(const Color &a, const Color &b) {
	return Color(a.R() / b.R(), a.G() / b.G(), a.B() / b.B());
}

Color operator+(const Color &a, double n) {
	return Color(a.R() + n, a.G() + n, a.B() + n);
}

Color operator*(const Color &a, double n) {
	return Color(a.R() * n, a.G() * n, a.B() * n);
}

Color operator/(const Color &a, double n) {
	return Color(a.R() / n, a.G() / n, a.B() / n);
}

Color operator+(double n, const Color &a) {
	return Color(a.R() + n, a.G() + n, a.B() + n);
}

Color operator*(double n, const Color &a) {
	return Color(a.R() * n, a.G() * n, a.B() * n);
}

Color operator/(double n, const Color &a) {
	return Color(a.R() / n, a.G() / n, a.B() / n);
}
