/*
 * Plane3d.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "Plane3d.h"

// constructor from vectors
Plane3d::Plane3d(const Vector3d& origin, const Vector3d& normal) {
    this->normal = normal;
    this->origin = origin;
    equation[0] = normal.X();
    equation[1] = normal.Y();
    equation[2] = normal.Z();
    equation[3] = -(normal.X()*origin.X()+normal.Y()*origin.Y()+normal.Z()*origin.Z());
}

// Construct from triangle:
Plane3d::Plane3d(const Vector3d& p1, const Vector3d& p2, const Vector3d& p3) {
    normal = (p2-p1) ^ (p3-p1);
    normal.normalize();
    origin = p1;
    equation[0] = normal.X();
    equation[1] = normal.Y();
    equation[2] = normal.Z();
    equation[3] = -(normal.X()*origin.X()+normal.Y()*origin.Y()+normal.Z()*origin.Z());
}

// deconstructor
Plane3d::~Plane3d() {
}

// isFrontFacingTo
bool Plane3d::isFrontFacingTo(const Vector3d& direction) const {
    double dot = normal * direction;
    return (dot <= 0);
}

// signedDistanceTo
double Plane3d::signedDistanceTo(const Vector3d& point) const {
    return (point * normal) + equation[3];
}

// getNormal
Vector3d Plane3d::getNormal() const {
    return normal;
}
