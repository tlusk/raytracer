/*
 * Plane3d.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef PLANE3D_H_
#define PLANE3D_H_

#include "Vector3d.h"

/**
 * Plane3d holds the information to describe a 3d plane
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Plane3d {

public:

    /**
     * Construct a Plane3d from an origin point and a normal vector
     * 
     * @param origin the point for the origin of the plane
     * @param vector vector describing the normal of the plane
     */
    Plane3d(const Vector3d& origin, const Vector3d& normal);

    /**
     * Construct a Plane3d using a triangle already on the plane
     * 
     * @param p1 point 1 on the plane
     * @param p2 point 2 on the plane
     * @param p3 point 3 on the plane
     */
    Plane3d(const Vector3d& p1, const Vector3d& p2, const Vector3d& p3);

    /**
     * Plane3d deconstructor
     */
    virtual ~Plane3d();

public:

    /**
     * Determine if the front of the plane is facing the passed vector
     * 
     * @param direction the direction of the vector passed
     * @return if the vector is facing the front of the plane
     */
    bool isFrontFacingTo(const Vector3d& direction) const;

    /**
     * Determine the signed distance to a point in space, from the plane
     * 
     * @param point the point to calculate the distance from
     * @return the signed distance to that point
     */
    double signedDistanceTo(const Vector3d& point) const;

    /**
     * Get the normal vector for the plane
     * 
     * @return the normal vector for the plane
     */
    Vector3d getNormal() const;

private:

    float equation[4];
    
    /**
     * Origin for a point on the plane
     */
    Point3d origin;
    
    /**
     * Normal vector for the plane
     */
    Vector3d normal;
};

#endif /*PLANE3D_H_*/
