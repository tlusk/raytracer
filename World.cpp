/*
 * World.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "World.h"
#include <cmath>

#define EPSILON  0.0000001
#define MAX_BOUNCES 4

// constructor
World::World() {
}

// deconstructor
World::~World() {
    for (size_t objNum = 0; objNum < objectList.size(); objNum++) {
        delete objectList.at(objNum);
    }
    for (size_t lightNum = 0; lightNum < lightList.size(); lightNum++) {
        delete lightList.at(lightNum);
    }
}

// add
void World::add(Object* obj) {
    objectList.push_back(obj);
}

// add
void World::add(Light* light) {
    lightList.push_back(light);
}

// spawn
Color World::spawn(Ray3d ray, int depth, bool internal) {
    Object* intersectionObject= NULL;
    Intersection intersectionData(-1, Point3d(0,0,0), Point3d(-1,-1,-1), Vector3d(-1, -1, -1));

    for (size_t objNum = 0; objNum < objectList.size(); objNum++) {
        Object* object = objectList.at(objNum);
        Intersection intersection = object->getIntersection(ray);
        double distance = intersection.getIntersectionDistance();
        if (distance >= 0) {
            if (intersectionData.getIntersectionDistance() == -1 || distance < intersectionData.getIntersectionDistance()) {
                intersectionObject = object;
                intersectionData = intersection;
            }
        }
    }

    if (intersectionObject != NULL) {

        Color returnColor;

        if (!internal) {
            std::vector<LightSource> visibleLights;

            for (size_t lightNum = 0; lightNum < lightList.size(); lightNum++) {
                Light* light = lightList.at(lightNum);
                std::vector<LightSource> lightSources = light->getLightSources();

                for (size_t lightSourceNum = 0; lightSourceNum < lightSources.size(); lightSourceNum++) {
                    LightSource lightSource = lightSources.at(lightSourceNum);

                    Vector3d lightRay = lightSource.getPosition() - intersectionData.getIntersectionPoint();
                    lightRay.normalize();

                    Ray3d shadowRay(intersectionData.getIntersectionPoint(), lightRay);

                    bool inShadow = false;
                    for (size_t objNum = 0; objNum < objectList.size() && !inShadow; objNum++) {
                        Object* object = objectList.at(objNum);
                        Intersection intersection = object->getIntersection(shadowRay);
                        if (intersection.getIntersectionDistance() >= EPSILON) {
                            double kTransmission = object->getTransmissionConstant();
                            if (kTransmission <= 0) {
                                inShadow = true;
                            } else {
                                Color lightColor = lightSource.getColor();
                                lightColor = lightColor * kTransmission;
                                lightSource.setColor(lightColor);
                            }
                        }
                    }

                    if ( !inShadow) {
                        visibleLights.push_back(lightSource);
                    }
                }
            }

            returnColor = intersectionObject->getColor(intersectionData, visibleLights);
        }

        if (depth < MAX_BOUNCES) {
            double kReflection = intersectionObject->getReflectionConstant();
            double kTransmission = intersectionObject->getTransmissionConstant();

            if (kReflection > 0) {
                Vector3d normal = intersectionData.getNormal();
                Vector3d cameraDirection = -ray.getDirection();
                Vector3d reflectionVector = 2 * normal * (cameraDirection * normal) - cameraDirection;
                reflectionVector.normalize();

                Ray3d reflectionRay(intersectionData.getIntersectionPoint(), reflectionVector);

                returnColor += kReflection * spawn(reflectionRay, depth+1, internal);
            }

            if (kTransmission > 0) {
                double internalRefraction = intersectionObject->getRefractionConstant();
                double externalRefraction = 1.0;

                Vector3d normal = intersectionData.getNormal();

                if (internal) {
                    std::swap(internalRefraction, externalRefraction);
                    normal = -normal;
                }

                Vector3d cameraDirection = ray.getDirection();

                double internalReflectionIndicator = 1 - (std::pow(externalRefraction, 2) * (1 - std::pow((cameraDirection * normal), 2)))/std::pow(internalRefraction, 2);

                Vector3d transmissionVector;

                if (internalReflectionIndicator > 0) {
                    Vector3d internalReflectionVector = normal * sqrt(internalReflectionIndicator);
                    transmissionVector = ((externalRefraction * (cameraDirection - (normal * (cameraDirection * normal))))/internalRefraction) - internalReflectionVector;
                } else {
                    cameraDirection = -cameraDirection;
                    cameraDirection.normalize();

                    transmissionVector = 2 * normal * (cameraDirection * normal) - cameraDirection;

                    internal = !internal;
                }

                transmissionVector.normalize();

                Ray3d transmissionRay(intersectionData.getIntersectionPoint(), transmissionVector);

                if (!internal) {
                    depth--;
                }

                returnColor += kTransmission * spawn(transmissionRay, depth+1, !internal);
            }
        }

        return returnColor;
    } else {
        return Color(0.5, 0.5, 1.0);
    }
}
