/*
 * Texture.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef TEXTURE_H_
#define TEXTURE_H_

#include "util/Color.h"
#include "util/Vector3d.h"

/**
 * Abstract Texture class used to define a texture that can be rendered onto an object
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Texture {

public:

    /**
     * Constructor for the texture
     */
    Texture();

    /**
     * Deconstructor for the texture
     */
    virtual ~Texture();

    /**
     * Gets the color at a specified intersection point
     * 
     * @param intersectionPoint the point to lookup the color
     * @return the color where we intersected
     */
    virtual Color getColor(Point3d intersectionPoint) const = 0;

};

#endif /*TEXTURE_H_*/
