/*
 * Sphere.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "Sphere.h"
#include <math.h>

#define EPSILON  0.0000001

// constructor
Sphere::Sphere(double x, double y, double z, double r, IlluminationModel* material, double kReflection, double kTransmission, double kRefraction) :
    Object(material, kReflection, kTransmission, kRefraction), center(x, y, z), radius(r) {
}

// deconstructor
Sphere::~Sphere() {
}

// getIntersection
Intersection Sphere::getIntersection(Ray3d ray) const {
    Point3d rayOrigin = ray.getOrigin();
    Vector3d rayDirection = ray.getDirection();

    double B = 2 * (rayDirection * (rayOrigin - center ) );

    // Temporary vector to calculate C with
    Vector3d CVector = rayOrigin - center;

    double C = (CVector * CVector ) - (radius * radius );

    double omegaTemp = B*B - 4*C;

    if (omegaTemp >= 0) {
        double omega1 = ((-B) + sqrt(omegaTemp))/2.0;
        double omega2 = ((-B) - sqrt(omegaTemp))/2.0;

        double omega = 0;
        if (omega1 < EPSILON) {
            omega = omega2;
        } else if (omega2 < EPSILON) {
            omega = omega1;
        } else {
            omega = std::min(omega1, omega2);
        }

        if (omega >= EPSILON) {
            Point3d intersectionPoint = rayOrigin + (rayDirection * omega);
            return Intersection(omega, rayOrigin, intersectionPoint, intersectionPoint - center);
        }
    }

    return Intersection(-1, Point3d(0,0,0), Point3d(-1,-1,-1), Vector3d(-1, -1, -1));
}
