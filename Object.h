/*
 * Object.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef OBJECT_H_
#define OBJECT_H_

#include "util/Ray3d.h"
#include "util/Color.h"
#include "IlluminationModel.h"

/**
 * Object class, defines an abstract object to place in the scene
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Object {

public:

    /**
     * Construct an object containing a refraction, reflection, and transmission constant
     * 
     * @param material The material the object is created from, contains texture and illumination
     * @param kReflection reflection constant for the object
     * @param kTransmission transmission constant for the object
     * @param kRefraction refraction constant for the object
     */
    Object(IlluminationModel* material, double kReflection, double kTransmission, double kRefraction);

    /**
     * Deconstruct the object
     */
    virtual ~Object();

    /**
     * Get the intersection point for the object
     * 
     * @param ray the to calculate an intersection with
     * @return intersection data about the collision
     */
    virtual Intersection getIntersection(Ray3d ray) const = 0;

    /**
     * Get the color of the object at the specified intersection
     * 
     * @param intersectionData intersection data used to find the color
     * @param lights the lights currently shining on the object
     * @return the color at that intersection
     */
    Color getColor(Intersection intersectionData, std::vector<LightSource> lights) const;

    /**
     * Get the reflection constant
     * 
     * @return the reflection constant
     */
    double getReflectionConstant();

    /**
     * Get the transmission constant
     * 
     * @return the transmission constant
     */
    double getTransmissionConstant();

    /**
     * Get the refraction constant
     * 
     * @return the refraction constant
     */
    double getRefractionConstant();

protected:

    /**
     * The material the object is composed from
     */
    IlluminationModel* material;

    /**
     * The reflection constant
     */
    double kReflection;

    /**
     * The transmission constant
     */
    double kTransmission;

    /**
     * The refraction constant
     */
    double kRefraction;

};

#endif /*OBJECT_H_*/
