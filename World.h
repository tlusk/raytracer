/*
 * World.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef WORLD_H_
#define WORLD_H_

#include <vector>
#include "Object.h"
#include "Light.h"
#include "util/Ray3d.h"

/**
 * World class, holds all the objects and lights in the scene that will be rendered
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class World {

public:

    /**
     * Constructor for the world object
     */
    World();

    /**
     * Deconstructor for the world object
     */
    virtual ~World();

    /**
     * Add an object to the world
     * 
     * @param obj the object to add to the world
     */
    void add(Object* obj);

    /**
     * Add a light to the world
     * 
     * @param light the light to add to the world
     */
    void add(Light* light);

    /**
     * Spawn a ray in the world and calculate the color of the object it hits
     * 
     * @param ray the ray to spawn
     * @param depth the number of bounces the ray may do
     * @param internal determine if the ray is inside an object currently
     * @return the final color of the scene where the ray intersects
     */
    Color spawn(Ray3d ray, int depth, bool internal);

private:

    /**
     * List of all the objects in the scene
     */
    std::vector<Object*> objectList;

    /**
     * List of all the lights in the scene
     */
    std::vector<Light*> lightList;

};

#endif /*WORLD_H_*/
