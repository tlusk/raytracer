/*
 * Intersection.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef INTERSECTION_H_
#define INTERSECTION_H_

#include "util/Vector3d.h"

/**
 * Intersection class, holds the data about an intersection in the world
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Intersection {

public:

    /**
     * Construct an intersection object
     * 
     * @param intersectionDistant the distance away the intersection occured
     * @param cameraPosition the position of the camera
     * @param intersectionPoint the coordinates where the intersection occured
     * @param normal the normal of the object being intersected with
     */
    Intersection(double intersectionDistance, Point3d cameraPosition, Point3d intersectionPoint, Vector3d normal);

    /**
     * Deconstruct the intersection object
     */
    virtual ~Intersection();

    /**
     * Get the point of intersection
     * 
     * @return the point of intersection
     */
    Point3d getIntersectionPoint() const;

    /**
     * Get the normal of the object being intersected with
     * 
     * @return the normal of the object being intersected with
     */
    Vector3d getNormal() const;

    /**
     * Get the vector for the viewing direction
     * 
     * @return the vector for the viewing direction
     */
    Vector3d getViewingDirection() const;

    /**
     * Get the distance away the intersection occured
     * 
     * @return the distance away the intersection occured
     */
    double getIntersectionDistance() const;

private:

    /**
     * The distance away the intersection occured
     */
    double intersectionDistance;

    /**
     * The point of intersection
     */
    Point3d intersectionPoint;

    /**
     * The vector for the viewing direction
     */
    Vector3d viewingDirection;

    /**
     * The normal of the object being intersected with
     */
    Vector3d normal;

};

#endif /*INTERSECTION_H_*/
