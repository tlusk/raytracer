/*
 * ImageTexture.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "ImageTexture.h"

// constructor
ImageTexture::ImageTexture(std::string imageName) :
    image(imageName) {
}

// deconstructor
ImageTexture::~ImageTexture() {
}

// getColor
Color ImageTexture::getColor(Point3d intersectionPoint) const {
    Point3d coordinateOrigin(-4.0, 0.0, -10.0);

    Point3d texturePoint = intersectionPoint - coordinateOrigin;

    double textureX = fmod(texturePoint.X()*200, image.getHeight());
    double textureY = fmod(texturePoint.Z()*200, image.getWidth());

    Color returnColor = image.getPixel((int)textureX, (int)textureY);

    return returnColor;
}
