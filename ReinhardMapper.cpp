/*
 * ReinhardMapper.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "ReinhardMapper.h"

// constructor
ReinhardMapper::ReinhardMapper(double lMax, double ldMax) :
    ToneMapper(lMax, ldMax) {
}

// deconstructor
ReinhardMapper::~ReinhardMapper() {
}

// compressColorLevels
Color ReinhardMapper::compressColorLevels(Color color, double lAvg) const {
    color *= ( 0.18 / lAvg );
    //color = color / ( 1 + color );
    color = color * ( 1 + color / 2.5 ) / ( 1 + color ); // Reinhard Mod
    color *= ldMax;

    return color;
}
