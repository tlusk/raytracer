/*
 * Camera.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "Camera.h"
#include <cmath>

// Distance of the virtual plane from the camera
#define VIRTUAL_PLANE_DISTANCE 1.0;

// constructor
Camera::Camera() :
    position(0, 0, 0), lookat(0, 0, 1), up(0, 1, 0), fov(45), aspect(1.3333) {
    generateTransformation(position, lookat, up);
}

// deconstructor
Camera::~Camera() {
}

// setLookAt
void Camera::setLookAt(double eyeX, double eyeY, double eyeZ, double centerX, double centerY, double centerZ, double upX, double upY, double upZ) {
    position = Point3d(eyeX, eyeY, eyeZ);
    lookat = Point3d(centerX, centerY, centerZ);
    up = Vector3d(upX, upY, upZ);

    generateTransformation(position, lookat, up);
}

// generateTransformation
void Camera::generateTransformation(Point3d position, Point3d lookat, Vector3d up) {
    Vector3d nVector = lookat - position;
    Vector3d vVector = up;
    Vector3d uVector = nVector ^ vVector;

    nVector.normalize();
    vVector.normalize();
    uVector.normalize();

    Point3d coordinateOrigin = position + nVector;

    double fovr = fov * M_PI/180.0;

    // Calculate the screen height and width
    screenHeight = tan(fovr / 2.0) * 2;
    screenWidth = screenHeight * aspect;

    Matrix4 newTransformationMatrix(uVector.X(), uVector.Y(), uVector.Z(), -coordinateOrigin * uVector, 
                                    vVector.X(), vVector.Y(), vVector.Z(), -coordinateOrigin * vVector, 
                                    nVector.X(), nVector.Y(), nVector.Z(), -coordinateOrigin * nVector, 
                                    0, 0, 0, 1);
    transformationMatrix = newTransformationMatrix.inverse();
}

// setFov
void Camera::setFov(double camerafov) {
    fov = camerafov;
}

// setAspectRatio
void Camera::setAspectRatio(double aspectRatio) {
    aspect = aspectRatio;
}

// spawnRay
Ray3d Camera::spawnRay(double xPercent, double yPercent) const {
    double cameraX = (-screenWidth / 2.0) + (screenWidth * xPercent);
    double cameraY = (-screenHeight / 2.0) + (screenHeight * yPercent);

    Vector4 cameraPoint(cameraX, cameraY, 0, 1);

    cameraPoint = transformationMatrix * cameraPoint;

    Vector3d rayVector = Vector3d(cameraPoint.x, cameraPoint.y, cameraPoint.z) - position;
    rayVector.normalize();

    Ray3d retRay(position, rayVector);

    return retRay;
}
