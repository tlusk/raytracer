/*
 * ToneMapper.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "ToneMapper.h"
#include <iostream>
#include <cmath>

#define EPSILON  0.0000001

// constructor
ToneMapper::ToneMapper(double lMax, double ldMax) :
    lMax(lMax), ldMax(ldMax) {
}

// deconstructor
ToneMapper::~ToneMapper() {

}

// processImage
Image ToneMapper::processImage(const Image &image) const {
    int xResolution = image.getXResolution();
    int yResolution = image.getYResolution();

    Image mappedImage(xResolution, yResolution);
    double lAvg = 0;

    // Scale the brightness and calculate average luminance
    for (int x = 0; x < xResolution; x++) {
        for (int y = 0; y < yResolution; y++) {
            Color pixelColor = image.getPixelColor(x, y);
            pixelColor *= lMax;

            lAvg += std::log(EPSILON + getLuminance(pixelColor));

            mappedImage.setPixel(x, y, pixelColor);
        }
    }

    lAvg = std::exp(lAvg * ( 1.0 / (double) (xResolution * yResolution) ));

    // Process the pixels
    for (int x = 0; x < xResolution; x++) {
        for (int y = 0; y < yResolution; y++) {
            Color pixelColor = mappedImage.getPixelColor(x, y);

            pixelColor = compressColorLevels(pixelColor, lAvg);

            pixelColor = pixelColor / ldMax;
            mappedImage.setPixel(x, y, pixelColor);
        }
    }

    return mappedImage;
}

// getLuminance
double ToneMapper::getLuminance(Color color) const {
    double luminance = (0.2126 * color.R()) + (0.7152 * color.G()) + (0.0722 * color.B());

    return luminance;
}
