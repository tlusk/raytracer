/*
 * CheckerboardTexture.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "CheckerboardTexture.h"

// constructor
CheckerboardTexture::CheckerboardTexture(Color color1, Color color2) :
    color1(color1), color2(color2) {
}

// deconstructor
CheckerboardTexture::~CheckerboardTexture() {
}

// getColor
Color CheckerboardTexture::getColor(Point3d intersectionPoint) const {
    Color returnColor;

    double squareSize = 0.5;

    Point3d coordinateOrigin(-4.0, 0.0, -10.0);

    Point3d texturePoint = intersectionPoint - coordinateOrigin;

    double textureX = fmod(texturePoint.X(), squareSize*2);
    double textureY = fmod(texturePoint.Z(), squareSize*2);

    if ((textureX >= squareSize && textureY >= squareSize) || (textureX < squareSize && textureY < squareSize)) {
        returnColor = color1;
    } else {
        returnColor = color2;
    }

    return returnColor;
}
