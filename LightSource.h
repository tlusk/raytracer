/*
 * LightSource.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef LIGHTSOURCE_H_
#define LIGHTSOURCE_H_

#include "util/Vector3d.h"
#include "util/Color.h"

/**
 * LightSource class, defines a light source containing color and position
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class LightSource {

public:

    /**
     * Constructs a light source object
     * 
     * @param position the position of the light source
     * @param color the color of the light source
     */
    LightSource(Point3d position, Color color);

    /**
     * Deconstructs a light source
     */
    virtual ~LightSource();

    /**
     * Gets the color of the light source
     * 
     * @return the color of the light source
     */
    Color getColor() const;

    /**
     * Gets the position of the light source
     * 
     * @return the position of the light source
     */
    Point3d getPosition() const;

    /**
     * Sets the color of the light source
     * 
     * @param newcolor the new color for the light source
     */
    void setColor(Color newcolor);

private:

    /**
     * The position of the light source
     */
    Point3d position;

    /**
     * The color of the light source
     */
    Color color;

};

#endif /*LIGHTSOURCE_H_*/
