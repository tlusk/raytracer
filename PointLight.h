/*
 * PointLight.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef POINTLIGHT_H_
#define POINTLIGHT_H_

#include "Light.h"

/**
 * Point Light class, defines a light that creates hard shadows
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class PointLight : public Light {

public:

    /**
     * Constructor for the point light
     * 
     * @param source the source where the light is coming from
     */
    PointLight(LightSource source);

    /**
     * Deconstructor for the point light
     */
    virtual ~PointLight();

    /**
     * Get the lightsources that are created by the point light
     */
    virtual std::vector<LightSource> getLightSources() const;

private:

    /**
     * Initial light source for the point light
     */
    LightSource source;

};

#endif /*POINTLIGHT_H_*/
