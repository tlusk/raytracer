/*
 * ToneMapper.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef TONEMAPPER_H_
#define TONEMAPPER_H_

#include "Image.h"

/**
 * Abstract class representing a tone mapper
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class ToneMapper {

public:

    /**
     * Constructor for the tone mapper
     * 
     * @param lMax the max luminance of the scene
     * @param ldMax the max luminance of the display device
     */
    ToneMapper(double lMax, double ldMax);

    /**
     * Deconstructor for the tone mapper
     */
    virtual ~ToneMapper();

    /**
     * Process the supplied image and tonemap it
     * 
     * @param image the supplied image
     * @return a tonemapped image
     */
    Image processImage(const Image &image) const;

private:

    /**
     * Get the total luminance from a passed color
     * 
     * @param color the color to extract the luminance from
     * @return the total luminance for the color
     */
    double getLuminance(Color color) const;

    /**
     * Compress the color levels for a specified color using the average luminance values
     * 
     * @param color the color to compress / tone map
     * @param lAvg the average luminance of the scene
     * @return a compressed / tone mapped color
     */
    virtual Color compressColorLevels(Color color, double lAvg) const = 0;

protected:

    /**
     * The max luminance of the scene
     */
    double lMax;

    /**
     * The max luminance of the display device
     */
    double ldMax;
};

#endif /*TONEMAPPER_H_*/
