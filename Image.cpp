/*
 * Image.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "Image.h"

// constructor
Image::Image(int xResolution, int yResolution) :
    xResolution(xResolution), yResolution(yResolution) {
    pixels = new float[xResolution*yResolution*3];

    // Clear pixels
    for (int i = 0; i < (xResolution*yResolution*3); i++) {
        pixels[i] = 0;
    }
}

// deconstructor
Image::~Image() {
    //delete[] pixels;
}

// setPixel
void Image::setPixel(int x, int y, Color color) {
    float *myPixel = getPixel(x, y);
    *myPixel++ = color.R();
    *myPixel++ = color.G();
    *myPixel++ = color.B();
}

// getPixel
float* Image::getPixel(int x, int y) const {
    return &pixels[(y*xResolution+x)*3];
}

// getPixelColor
Color Image::getPixelColor(int x, int y) const {
    float *myPixel = getPixel(x, y);

    double r = *myPixel++;
    double g = *myPixel++;
    double b = *myPixel++;

    return Color(r, g, b);
}

// getPixels
float* Image::getPixels() const {
    return pixels;
}

// getYResolution
int Image::getYResolution() const {
    return yResolution;
}

// getXResolution
int Image::getXResolution() const {
    return xResolution;
}
