/*
 * AreaLight.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef AREALIGHT_H_
#define AREALIGHT_H_

#include "Light.h"

/**
 * Area Light class, defines a light that creates softer shadows
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class AreaLight : public Light {

public:

    /**
     * Constructor for the area light
     * 
     * @param source the source where the light is coming from
     * @param radius size of the spread for the light vectors
     */
    AreaLight(LightSource source, double radius);

    /**
     * Deconstructor for the AreaLight class
     */
    virtual ~AreaLight();

    /**
     * Get the lightsources that are created by the area light
     */
    virtual std::vector<LightSource> getLightSources() const;

private:

    /**
     * Initial light source for the area light
     */
    LightSource source;

    /**
     * Radius of the light source
     */
    double radius;

};

#endif /*AREALIGHT_H_*/
