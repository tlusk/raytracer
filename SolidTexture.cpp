/*
 * SolidTexture.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "SolidTexture.h"

// constructor
SolidTexture::SolidTexture(Color color) :
    color(color) {
}

// deconstructor
SolidTexture::~SolidTexture() {
}

// getColor
Color SolidTexture::getColor(Point3d intersectionPoint) const {
    return color;
}
