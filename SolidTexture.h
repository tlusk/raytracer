/*
 * SolidTexture.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef SOLIDTEXTURE_H_
#define SOLIDTEXTURE_H_

#include "Texture.h"

/**
 * Solid Texture class, defines a single color texture
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class SolidTexture : public Texture {

public:

    /**
     * Constructor for the texture
     * 
     * @param color the color of the texture
     */
    SolidTexture(Color color);

    /**
     * Deconstructor for the texture
     */
    virtual ~SolidTexture();

    /**
     * Gets the color at a specified intersection point
     * 
     * @param intersectionPoint the point to lookup the color
     * @return the color where we intersected
     */
    virtual Color getColor(Point3d intersectionPoint) const;

private:

    /**
     * The color of the texture
     */
    Color color;

};

#endif /*SOLIDTEXTURE_H_*/
