/*
 * PhongModel.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "PhongModel.h"
#include <cmath>

// constructor
PhongModel::PhongModel(Texture* objectTexture, double ambientCoefficient, double diffuseCoefficient, double specularCoefficient, double shine) :
    IlluminationModel(objectTexture), ambientCoefficient(ambientCoefficient), diffuseCoefficient(diffuseCoefficient), specularCoefficient(specularCoefficient), shine(shine) {
}

// constructor
PhongModel::PhongModel(Texture* ambientTexture, Texture* diffuseTexture, Texture* specularTexture, double ambientCoefficient, double diffuseCoefficient, double specularCoefficient, double shine) :
    IlluminationModel(ambientTexture, diffuseTexture, specularTexture), ambientCoefficient(ambientCoefficient), diffuseCoefficient(diffuseCoefficient), specularCoefficient(specularCoefficient),
            shine(shine) {
}

// deconstructor
PhongModel::~PhongModel() {
}

// illuminate
Color PhongModel::illuminate(Intersection intersectionData, std::vector<LightSource> lights) const {
    Vector3d normal = intersectionData.getNormal();

    Vector3d viewingDirection = intersectionData.getViewingDirection();

    Color newColor = ambientCoefficient * getAmbientTexture()->getColor(intersectionData.getIntersectionPoint());

    Color newDiffuseColor;
    Color newSpecularColor;

    for (size_t lightNum = 0; lightNum < lights.size(); lightNum++) {
        LightSource light = lights.at(lightNum);
        Color lightColor = light.getColor();

        Vector3d lightRay = light.getPosition() - intersectionData.getIntersectionPoint();
        lightRay.normalize();

        Vector3d reflectionRay = 2 * normal * (lightRay * normal) - lightRay;
        reflectionRay.normalize();

        newDiffuseColor += getDiffuseTexture()->getColor(intersectionData.getIntersectionPoint()) * lightColor * (lightRay * normal);
        newSpecularColor += getSpecularTexture()->getColor(intersectionData.getIntersectionPoint()) * lightColor * std::pow(std::max(reflectionRay * viewingDirection, 0.0), shine);
    }

    newColor += diffuseCoefficient * newDiffuseColor + specularCoefficient * newSpecularColor;

    return newColor;
}
