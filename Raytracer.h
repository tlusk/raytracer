/*
 * Raytracer.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef RAYTRACER_H_
#define RAYTRACER_H_

#include "Video.h"
#include "Renderer.h"
#include "World.h"

/**
 * Raytracer class, controls the raytracing engine
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Raytracer {

public:

    /**
     * Constructor for the raytracer class
     */
    Raytracer(int xResolution, int yResolution, int numSuperSamples);

    /**
     * Deconstructor for the raytracer class
     */
    virtual ~Raytracer();

    /**
     * Initializes the raytracer
     */
    void init();

    /**
     * Run the raytracer
     */
    void run();

private:

    /**
     * Video class, initializes the SDL screen
     */
    Video* video;

    /**
     * World Scene
     */
    World* world;

    /**
     * Raytracing renderer class
     */
    Renderer* renderer;

};

#endif /*RAYTRACER_H_*/
