/*
 * main.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "Raytracer.h"
#include <fenv.h>
#ifdef WIN32
#include <windows.h>
#endif

// Number of subsamples to take when rendering
#define NUM_SUPERSAMPLES 1;
// X and Y Resolution of the image to render
#define X_RESOLUTION 640;
#define Y_RESOLUTION 480;

/**
 * Raytracer main class
 * 
 * @author      Timothy Lusk
 *
 * @param       argc   the number of command line arguments
 * @param       argv   an array holding the comand line arguments
 */
#ifdef WIN32
// Windows
int WINAPI WinMain (HINSTANCE instance, HINSTANCE, PSTR, int iCmdShow) {
#else
int main(int argc, char *argv[]) {
#endif
    //feenableexcept(FE_INVALID|FE_DIVBYZERO);

    int numSuperSamples= NUM_SUPERSAMPLES;
    int xResolution= X_RESOLUTION;
    int yResolution= Y_RESOLUTION;

    Raytracer raytracer(xResolution, yResolution, numSuperSamples);

    raytracer.run();

    return 0;
}
