/*
 * IlluminationModel.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef ILLUMINATIONMODEL_H_
#define ILLUMINATIONMODEL_H_

#include <vector>
#include "Intersection.h"
#include "LightSource.h"
#include "Texture.h"

/**
 * Abstract class used to define illumination models
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class IlluminationModel {

public:

    /**
     * Construct the illumination model from a texture, will also be used as the diffuse texture, specular will be set to solid white
     * 
     * @param objectTexture the texture to use for this object
     */
    IlluminationModel(Texture* objectTexture);

    /**
     * Construct an illumination model from an ambient, diffuse, and specular texture
     * 
     * @param ambientTexture ambientTexture to use for this illumination
     * @param diffuseTexture diffuseTexture to use for this illumination
     * @param specularTexture specularTexture to use for this illumination
     */
    IlluminationModel(Texture* ambientTexture, Texture* diffuseTexture, Texture* specularTexture);

    /**
     * Deconstructor for the illumination model, destroys the textures
     */
    virtual ~IlluminationModel();

    /**
     * Returns the illuminated color at a specified intersection with passed light sources
     * 
     * @param intersectionData the intersection point
     * @param lights the lights acting on the intersection point
     * @return the color of the illuminated point
     */
    virtual Color illuminate(Intersection intersectionData, std::vector<LightSource> lights) const = 0;

protected:

    /**
     * Returns the ambient texture used by this illumination model
     * 
     * @return the ambient texture used by this illumination model
     */
    Texture* getAmbientTexture() const;

    /**
     * Returns the diffuse texture used by this illumination model
     * 
     * @return the diffuse texture used by this illumination model
     */
    Texture* getDiffuseTexture() const;

    /**
     * Returns the specular texture used by this illumination model
     * 
     * @return the specular texture used by this illumination model
     */
    Texture* getSpecularTexture() const;

protected:

    /**
     * The ambient texture used by this illumination model
     */
    Texture* ambientTexture;

    /**
     * The diffuse texture used by this illumination model
     */
    Texture* diffuseTexture;

    /**
     * The specular texture used by this illumination model
     */
    Texture* specularTexture;

};

#endif /*ILLUMINATIONMODEL_H_*/
