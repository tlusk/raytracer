/*
 * ReinhardMapper.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef REINHARDMAPPER_H_
#define REINHARDMAPPER_H_

#include "ToneMapper.h"

/**
 * Tone mapper using the reinhard algorithm
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class ReinhardMapper : public ToneMapper {

public:

    /**
     * Constructor for the tone mapper
     * 
     * @param lMax the max luminance of the scene
     * @param ldMax the max luminance of the display device
     */
    ReinhardMapper(double lMax, double ldMax);

    /**
     * Deconstructor for the tone mapper
     */
    virtual ~ReinhardMapper();

private:

    /**
     * Compress the color levels for a specified color using the average luminance values
     * 
     * @param color the color to compress / tone map
     * @param lAvg the average luminance of the scene
     * @return a compressed / tone mapped color
     */
    virtual Color compressColorLevels(Color color, double lAvg) const;

};

#endif /*REINHARDMAPPER_H_*/
