/*
 * Image.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef IMAGE_H_
#define IMAGE_H_

#include "util/Color.h"

/**
 * Image class, holds a series of pixels representing the raytraced image
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Image {

public:

    /**
     * Construct an image class containing a black image
     * 
     * @param xResolution horizontal resolution of the image
     * @param yResolution vertical resolution of the image
     */
    Image(int xResolution, int yResolution);

    /**
     * Deconstruct the image class
     */
    virtual ~Image();

    /**
     * Set the specified pixel to a specified color
     * 
     * @param x the x coordinate for the pixel
     * @param y the y coordinate for the pixel
     * @param color the color to set the pixel to
     */
    void setPixel(int x, int y, Color color);

    /**
     * Get a pointer to the pixel at a specified location
     * 
     * @param x the x coordinate for the pixel
     * @param y the y coordinate for the pixel
     * @return a pointer containing the memory address for specified pixel location
     */
    float* getPixel(int x, int y) const;

    /**
     * Get the color of a pixel at a specified location
     * 
     * @param x the x coordinate for the pixel
     * @param y the y coordinate for the pixel
     * @return the color of the pixel at the specified location
     */
    Color getPixelColor(int x, int y) const;

    /**
     * Get a pointer to the array of pixels
     * 
     * @return a pointer to the array of pixels
     */
    float* getPixels() const;

    /**
     * Get the horizontal resolution for the image
     * 
     * @return the horizontal resolution for the image
     */
    int getXResolution() const;

    /**
     * Get the vertical resolution for the image
     * 
     * @return the vertical resolution for the image
     */
    int getYResolution() const;

private:

    /**
     * The horizontal resolution for the image
     */
    int xResolution;

    /**
     * The vertical resolution for the image
     */
    int yResolution;

    /**
     * The pixels stored within this image
     */
    float *pixels;

};

#endif /*IMAGE_H_*/
