/*
 * CheckerboardTexture.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef CHECKERBOARDTEXTURE_H_
#define CHECKERBOARDTEXTURE_H_

#include "Texture.h"

/**
 * Checkerboard Texture class, defines a procedural texture to create a checkerboard
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class CheckerboardTexture : public Texture {

public:

    /**
     * Constructor for the checkerboard class
     * 
     * @param color1 first color on the checkerboard
     * @param color2 second color on the checkerboard
     */
    CheckerboardTexture(Color color1, Color color2);

    /**
     * Checkerboard class deconstructor
     */
    virtual ~CheckerboardTexture();

    /**
     * Gets the color on the checkerboard at a specified intersection point
     * 
     * @param intersectionPoint the point to lookup the color
     * @return the color where we intersected
     */
    virtual Color getColor(Point3d intersectionPoint) const;

private:

    /**
     * First color on the checkerboard
     */
    Color color1;

    /**
     * Second color on the checkerboard
     */
    Color color2;

};

#endif /*CHECKERBOARDTEXTURE_H_*/
