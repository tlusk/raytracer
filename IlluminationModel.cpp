/*
 * IlluminationModel.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "IlluminationModel.h"
#include "SolidTexture.h"

// constructor
IlluminationModel::IlluminationModel(Texture* objectTexture) :
    ambientTexture(objectTexture), diffuseTexture(NULL), specularTexture(new SolidTexture(Color(1.0, 1.0, 1.0))) {
}

// constructor
IlluminationModel::IlluminationModel(Texture* ambientTexture, Texture* diffuseTexture, Texture* specularTexture) :
    ambientTexture(ambientTexture), diffuseTexture(diffuseTexture), specularTexture(specularTexture) {
}

// decontructor
IlluminationModel::~IlluminationModel() {
    delete ambientTexture;
    delete specularTexture;
    if (diffuseTexture) {
        delete diffuseTexture;
    }
}

// getAmbientTexture
Texture* IlluminationModel::getAmbientTexture() const {
    return ambientTexture;
}

// getDiffuseTexture
Texture* IlluminationModel::getDiffuseTexture() const {
    if (diffuseTexture) {
        return diffuseTexture;
    } else {
        return ambientTexture;
    }
}

// getSpecularTexture
Texture* IlluminationModel::getSpecularTexture() const {
    return specularTexture;
}
