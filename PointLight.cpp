/*
 * PointLight.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "PointLight.h"

// constructor
PointLight::PointLight(LightSource source) :
    source(source) {
}

// deconstructor
PointLight::~PointLight() {
}

// getLightSources
std::vector<LightSource> PointLight::getLightSources() const {
    std::vector<LightSource> lightSources;
    lightSources.push_back(source);
    return lightSources;
}
