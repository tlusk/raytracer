/*
 * Polygon.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef POLYGON_H_
#define POLYGON_H_

#include <vector>
#include "Object.h"
#include "util/Vector3d.h"
#include "util/Plane3d.h"

/**
 * Polygon class, defines a polygon object to place in the scene
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Polygon : public Object {

public:

    /**
     * Construct a polygon object from points, containing a refraction, reflection, and transmission constant
     * 
     * @param v0 first point on the polygon
     * @param v1 second point on the polygon
     * @param v2 third point on the polygon
     * @param v3 fourt point on the polygon
     * @param material The material the object is created from, contains texture and illumination
     * @param kReflection reflection constant for the object
     * @param kTransmission transmission constant for the object
     * @param kRefraction refraction constant for the object
     */
    Polygon(Point3d v0, Point3d v1, Point3d v2, Point3d v3, IlluminationModel* material, double kReflection, double kTransmission, double kRefraction);

    /**
     * Deconstruct the polygon
     */
    virtual ~Polygon();

    /**
     * Get the intersection point for the polygon
     * 
     * @param ray the to calculate an intersection with
     * @return intersection data about the collision
     */
    virtual Intersection getIntersection(Ray3d ray) const;

private:

    /**
     * Calculate the sum of all the angles from a point to other points
     * 
     * @param q the point to calculate from
     * @param points the other points to use to calculate the angles from
     * @return the sun of all the angles
     */
    double CalcAngleSum(Point3d q, std::vector<Point3d> points) const;

    /**
     * The vertices for the polygon
     */
    std::vector<Point3d> vertices;

    /**
     * The plane the polygon sits upon
     */
    Plane3d plane;

};

#endif /*POLYGON_H_*/
