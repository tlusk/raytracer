/*
 * Video.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef VIDEO_H_
#define VIDEO_H_

#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>

/**
 * Video class, initializes the video for the raytracer
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Video {

public:

    /**
     * Constructor for the video class
     * 
     * @param h_rez the horiziontal resolution to initialize
     * @param v_rez the vertical resolution to initialize
     */
    Video(int h_rez, int v_rez);

    /**
     * Deconstructor for the video class
     */
    virtual ~Video();

private:

    /**
     * Initializes Opengl
     * 
     * @param width screen width
     * @param height screen height
     */
    void init_opengl(long width, long height);

    /**
     * Initializes SDL
     * 
     * @param width screen width
     * @param height screen height
     * @returns SDL Screen object
     */
    SDL_Surface *init_sdl(long width, long height);

    /**
     * SDL Surface which is rendered to
     */
    SDL_Surface *surf;

};

#endif /*VIDEO_H_*/
