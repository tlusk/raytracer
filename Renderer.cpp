/*
 * Renderer.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "Renderer.h"
#include "Camera.h"
#include "WardMapper.h"
#include "ReinhardMapper.h"

// Define lMax and ldMax
#define LMAX 10000.0
#define LDMAX 1000.0
// Define the type of tone mapping (can be neither)
#define WARD
//#define REINHARD

// constrcutor
Renderer::Renderer(World &world, int xResolution, int yResolution, int numSuperSamples) :
    T0(0), Frames(0), fps(0), world(world), xResolution(xResolution), yResolution(yResolution), image(xResolution, yResolution), numSuperSamples(numSuperSamples) {
}

// deconstructor
Renderer::~Renderer() {
}

void Renderer::trace() {
    Camera camera;
    camera.setLookAt(-2, 1, 7, -2, 0, 0, 0, 1, 0);
    camera.setAspectRatio(xResolution/(double)yResolution);

    for (int y = 0; y < yResolution; y++) {
        for (int x = 0; x < xResolution; x++) {

            double r = 0;
            double g = 0;
            double b = 0;

            for (int superSampleX = 0; superSampleX < numSuperSamples; superSampleX++) {
                for (int superSampleY = 0; superSampleY < numSuperSamples; superSampleY++) {

                    double superSampleStepX = -0.5+(superSampleX/(double)numSuperSamples);
                    double superSampleStepY = -0.5+(superSampleY/(double)numSuperSamples);

                    double xPercentage = (x+superSampleStepX)/(double)xResolution;
                    double yPercentage = (y+superSampleStepY)/(double)yResolution;

                    Ray3d ray = camera.spawnRay(xPercentage, yPercentage);

                    Color intersectionColor = world.spawn(ray, 1, false);

                    r += intersectionColor.R();
                    g += intersectionColor.G();
                    b += intersectionColor.B();
                }
            }

            int numberOfSamples = numSuperSamples * numSuperSamples;

            Color averageColor(r/(double)numberOfSamples, g/(double)numberOfSamples, b/(double)numberOfSamples);

            image.setPixel(x, y, averageColor);
        }

        // Preview each scanline
        glDrawPixels(xResolution, yResolution, GL_RGB, GL_FLOAT, image.getPixels());
        SDL_GL_SwapBuffers();
    }

#ifdef WARD
    WardMapper toneMapper(LMAX, LDMAX);
    image = toneMapper.processImage(image);
#elif REINHARD
    ReinhardMapper toneMapper(LMAX, LDMAX);
    image = toneMapper.processImage(image);
#endif
}

// render
void Renderer::render() const {
    // Draw the image onto the screen
    glDrawPixels(xResolution, yResolution, GL_RGB, GL_FLOAT, image.getPixels());

    // Swap buffers to display the scene
    SDL_GL_SwapBuffers();
}

// calculateFps
void Renderer::calculateFps() {
    // Gather our frames per second
    Frames++;
    GLint t = SDL_GetTicks();
    if (t - T0 >= 1000) {
        // Calculate the FPS
        GLfloat seconds = (t - T0) / 1000.0;
        fps = Frames / seconds;
        T0 = t;
        Frames = 0;

        // Set the fps into the title
        static char buffer[20] = { 0 };
        sprintf(buffer, "CG2 Raytracer - %f FPS", fps);
        SDL_WM_SetCaption(buffer, 0);
    }
}
