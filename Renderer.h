/*
 * Renderer.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef RENDERER_H_
#define RENDERER_H_

#include "Video.h"
#include "World.h"
#include "Image.h"

/**
 * Renderer class, renders the scene
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Renderer {

public:

    /**
     * Constructor for the renderer class
     */
    Renderer(World& world, int xResolution, int yResolution, int numSuperSamples);

    /**
     * Deconstructor for the renderer class
     */
    virtual ~Renderer();

    /**
     * Renderer's the scene
     */
    void render() const;

    /**
     * Raytrace the world
     */
    void trace();

    /**
     * Calculate the frames per second
     */
    void calculateFps();

private:

    /**
     * FPS Calculation Data
     */
    GLint T0;

    /**
     * Frames counted between intervals
     */
    GLint Frames;

    /**
     * The fps of the scene
     */
    GLfloat fps;

    /**
     * World object holding the objects in the scene to render
     */
    World& world;

    /**
     * Horizontal resolution of the image to render
     */
    int xResolution;

    /**
     * Vertical resolution of the image to render
     */
    int yResolution;

    /**
     * The image to render to and display
     */
    Image image;

    /**
     * The number of sub samples to use when rendering
     */
    int numSuperSamples;

};

#endif /*RENDERER_H_*/
