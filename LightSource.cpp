/*
 * LightSource.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "LightSource.h"

// constructor
LightSource::LightSource(Point3d position, Color color) :
    position(position), color(color) {
}

// deconstructor
LightSource::~LightSource() {
}

// getColor
Color LightSource::getColor() const {
    return color;
}

// getPosition
Point3d LightSource::getPosition() const {
    return position;
}

// setColor
void LightSource::setColor(Color newcolor) {
    color = newcolor;
}
