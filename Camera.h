/*
 * Camera.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#include "util/Vector3d.h"
#include "util/Ray3d.h"
#include "util/Matrix4.h"

/**
 * Area Light class, defines a light that creates softer shadows
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class Camera {

public:

    /**
     * Constructor for the camera
     */
    Camera();

    /**
     * Deconstructor for the camera
     */
    virtual ~Camera();

    /**
     * Generates the transformation matrix which is used by the camera to transform world to camera space
     * 
     * @param position the position of the camera
     * @param lookat the lookat vector for the camera, defines where the camera is looking
     * @param up the up vector, defines what the camera sees as being up
     */
    void generateTransformation(Point3d position, Point3d lookat, Vector3d up);

    /**
     * Sets the lookat for the camera using the same syntax as the opengl command
     * 
     * @param eyeX the x coordinate for the camera eye
     * @param eyeY the y coordinate for the camera eye
     * @param eyeZ the z coordinate for the camera eye
     * @param centerX the x coordinate for what the camera is looking at
     * @param centerX the y coordinate for what the camera is looking at
     * @param centerX the z coordinate for what the camera is looking at
     * @param upX the x coordinate for what the camera sees at being up in the world
     * @param upY the y coordinate for what the camera sees at being up in the world
     * @param upZ the z coordinate for what the camera sees at being up in the world
     */
    void setLookAt(double eyeX, double eyeY, double eyeZ, double centerX, double centerY, double centerZ, double upX, double upY, double upZ);

    /**
     * Sets the degrees fov for the world
     * 
     * @param fov the degrees fov for the world
     */
    void setFov(double fov);

    /**
     * Sets the aspect ratio of the world
     * 
     * @param aspectRatio the aspect ratio of the world
     */
    void setAspectRatio(double aspectRatio);

    /**
     * Spawns a ray a specified percentage away from the bottom left corner in the world
     * 
     * @param xPercent the percentage in the cameras view from the left side of the screen
     * @param yPercent the percentage in the cameras view from the bottom of the screen
     * @return a ray spawned through the camera and through the point specified by the x and y percentages
     */
    Ray3d spawnRay(double xPercent, double yPercent) const;

private:

    /**
     * The position of the camera
     */
    Point3d position;

    /**
     * The lookat point for the camera
     */
    Point3d lookat;

    /**
     * The up vector for the camera
     */
    Vector3d up;

    /**
     * The field of view for the camera
     */
    double fov;

    /**
     * The aspect ratio of the camera
     */
    double aspect;

    /**
     * The height of the virtual screen
     */
    double screenHeight;

    /**
     * The width of the virtual screen
     */
    double screenWidth;

    /**
     * The transformation matrix used by the camera to convert to and from camera space
     */
    Matrix4 transformationMatrix;

};

#endif /*CAMERA_H_*/
