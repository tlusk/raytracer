/*
 * ImageTexture.h
 *
 * Version:
 *      $Id$
 *
 */

#ifndef IMAGETEXTURE_H_
#define IMAGETEXTURE_H_

#include <iostream>
#include "Texture.h"
#include "util/BmpFile.h"

/**
 * Image Texture class, holds a texture containing an image file which can be used to texture objects when rendering
 *
 * @version     $Id$
 *
 * @author      Timothy Lusk
 */
class ImageTexture : public Texture {

public:

    /**
     * Create the Image Texture from an image location
     * 
     * @param imageName absolute location for the image file
     */
    ImageTexture(std::string imageName);

    /**
     * Deconstruct the image texture
     */
    virtual ~ImageTexture();

    /**
     * Gets the color of the image at a specified intersection point
     */
    virtual Color getColor(Point3d intersectionPoint) const;

private:

    /**
     * Bitmap image file to use when determining color
     */
    BmpFile image;

};

#endif /*IMAGETEXTURE_H_*/
