/*
 * Object.cpp
 *
 * Version:
 *      $Id$
 *
 */

#include "Object.h"

// constructor
Object::Object(IlluminationModel* material, double kReflection, double kTransmission, double kRefraction) :
    material(material), kReflection(kReflection), kTransmission(kTransmission), kRefraction(kRefraction) {
}

// deconstructor
Object::~Object() {
    delete material;
}

// getColor
Color Object::getColor(Intersection intersectionData, std::vector<LightSource> lights) const {
    return material->illuminate(intersectionData, lights);
}

// getReflectionConstant
double Object::getReflectionConstant() {
    return kReflection;
}

// getTransmissionConstant
double Object::getTransmissionConstant() {
    return kTransmission;
}

// getRefractionConstant
double Object::getRefractionConstant() {
    return kRefraction;
}
